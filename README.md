Here you can find a demo of a Python application with an automated testing using Bitbucket Pipelines service.

Anytime you want to start a new project in Python, feel free to use this code as a skeleton.
Copy the contents to your repository and build your code upon it.

File demo.py contains a code for factorial() function.

File test_demo.py has a few tests to make sure the factorial() function works correctly.

To test the code from command line, run:

    pytest -v

After every commit, visit the Pipelines tab and review the results of automated linting and testing.

Prerequisites:

    pip3 install pytest flake8

Links:

 * https://realpython.com/pytest-python-testing/
 * https://support.atlassian.com/bitbucket-cloud/docs/python-with-bitbucket-pipelines/
 * https://flake8.pycqa.org/en/latest/
